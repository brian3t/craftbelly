curl -s https://shopify.dev/themekit.py | sudo python3

to get theme list: 
theme get --list --password=shptka_65c268e1d941389151c73173e64b4967 --store="craftbelly-delights-inc.myshopify.com"

LIVE theme:
theme get --password=shptka_65c268e1d941389151c73173e64b4967 --store="craftbelly-delights-inc.myshopify.com" --themeid=131086090399   
(or run ```tgl``` ; or run ```t```)

#### To upload live theme:

theme --allow-live watch

#### To pull live theme:

```tgl```

####To preview theme:
https://craftbellydelights.com/?_ab=0&_fd=0&_sc=1&preview_theme_id=131086090399

Preview product:
https://hwyrziv31tzuw2c8-42630709407.shopifypreview.com/products_preview?preview_key=1f7ba900b90ea4e080b4167a5b8e4cce&preview_theme_id=131086090399
