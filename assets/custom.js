/**
 * Custom code for CraftBelly Fresh theme
 * 8/21/22 BN Added
 */
jQuery(function ($){
  console.log(`BN custom start`)
  window.tutil = new ThemeUtil()
 window.circularText = function (txt, radius, e) {
  txt = txt.split("")
   if (txt.length == 0) return false

  const deg = 360 / txt.length
  let origin = 0;

  txt.forEach((ea) => {
    ea = `<p style='height:${radius}px;position:absolute;transform:rotate(${origin}deg);transform-origin:0 100%'>${ea}</p>`;
    e.innerHTML += ea;
    origin += deg;
  });
}
  if (tutil.isMobile()) {
    $('.homepage-sections-wrapper div.row.grid--blog').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 1500,
      dots: true,
      arrows: false
    })
    $('.homepage-featured-products').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 15000,
      dots: true,
      arrows: false
    })
    $('.testimonial-slides').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 7000,
      dots: true,
      arrows: false
    })
  }
  tutil.bind_add_to_cart()

  $('.js-home-testimonials').slick({infinite: true, dots: true, arrows: false})
  $('.prod_1st_tag').each((i, e)=>{
    new CircleType(e)
    // const the_tag = $(e).text()
    // $(e).text('')
    // circularText(the_tag, 50, e)
  })
  //location page
  $('#states').on('change', (e)=>{
    e.preventDefault()
    e.stopPropagation()
    const tag_handle = $(e.target).find(':selected').val()
    console.log(`value`, tag_handle)
    window.open(`/blogs/locations/tagged/${tag_handle}`, '_blank')
  })
  //location page end
  //Hide class="video" desktop if desktop
  if (detect_mob())
  {
    console.log(`is mobile, deleting desktop video`)
    $('div.hero-video--media.hero-video--mp4 > div.video').remove()
    $('div.hero-video--text').hide()
    setTimeout(() => {$('#home_mobile_vid')[0].play()}, 500)
  } else {
    $('#home_mobile_vid').remove()
  }

  const cur_page = window.location.href
  //Highlight current blog tag; e.g. when current url is blogs/recipes/tagged/tag_name_here
  if (cur_page.includes('/tagged/')){
    const cur_tag = cur_page.split('/').pop()
    console.log(cur_tag)
    const anchor_cur_page = $(`a[href$="tagged/${cur_tag}"]`) //anchor that points to current page
    if (anchor_cur_page.length == 1){
      anchor_cur_page.addClass('point_to_cur_page')
    }
  }
})

/**
 * Utilities for the Fresh theme
 * 8/22/22 BrianN created. Usage:
 * addToCart
 */
class ThemeUtil {
  constructor(){
    this.config = {
      shopifyAjaxAddUrl: '/cart/add.js',
      shopifyAjaxUrl: '/cart.js'
    }
    this.translations = {};
    this.moneyFormat = '${{amount}}';
    this.breakpoint = void 0;
    this.cartDrawerStatus = document.querySelector('.js-cart-drawer-status');

    let _this;

    _this = this;

    this.isMobile = () => {
      return window.matchMedia('only screen and (max-width: 1007px)').matches;
    };

    this.bind_add_to_cart = function (){
      $('.btn_add_cart').on('click', this.addToCartAjax)
    }

    /**
     * Add to cart ajax. This function detects the current product's form. In this fashion, we can have multiple add-to-cart buttons on the same page
     * 8/22/22 BN created
     * @param e Event
     * @returns {Promise<boolean>}
     */
    this.addToCartAjax = async function (e){
      e.preventDefault()
      e.stopPropagation()
      _this.clearError();

      let self = _this;
      const $closest_form = $(e.target).closest('form')
      if ($closest_form.length != 1) {
        console.error(`Cannot find add to cart form. Double check html`)
        return false
      }
      _this.addToCartForm = $closest_form[0]

      const formData = new FormData(_this.addToCartForm)

      try {
        const fetchResult = await fetch(_this.config.shopifyAjaxAddUrl, {
          method: 'POST',
          headers: {
            'x-requested-with': 'XMLHttpRequest'
          },
          credentials: 'same-origin',
          body: formData
        });
        const responseJson = await fetchResult.json();

        if (! fetchResult.ok) {
          _this.addToCartFailed(responseJson);
        } else {
          _this.addedToCart();
        }
      } catch (error) {
        console.error(error);
      }
    }

    this.addedToCart = itemData => {
      this.updateCart();
    };

    this.openCartPage = () => {
      // window.location.href = this.addToCartForm.querySelector('#cart-link').value.toString();
    };

    this.showCart = () => {
      const cartDrawerTop = document.querySelector('.cart-drawer__top');
      let cartDrawerStatus = document.querySelector('.js-cart-drawer-status');

      cartDrawerStatus.textContent = `Item added to your cart`;
      cartDrawerTop.appendChild(cartDrawerStatus);
      const event = new CustomEvent('cart:open');
      document.querySelector('#cartSlideoutWrapper').dispatchEvent(event);
    };
    this.updateCart = async function (){
      try {
        const getResult = await fetch(_this.config.shopifyAjaxUrl, {
          method: 'GET',
          headers: {
            'x-requested-with': 'XMLHttpRequest'
          },
          credentials: 'same-origin'
        });
        const responseJson = await getResult.json();

        if (! getResult.ok) {
          // _this.updateCartFailed(getResult);
        } else {
          _this.updatedCart(responseJson);
        }
      } catch (error) {
        console.error(error);
      }
    }

    this.updatedCart = data => {
      const cart_element = document.querySelector('body')
      this.updateCartDrawer(data, cart_element)
      // this.enableCartButton();
      // const cart_action = document.getElementById('PageContainer').dataset.cartAction;
      // const cart_type = document.getElementById('PageContainer').dataset.cartType;

      // if (cart_action === 'cart' && cart_type == 'page') {
      this.openCartPage();
      // } else if (cart_action === 'added') {
      //   this.cart.innerHTML = this.theme.translations.added;
      //
      //   if (this.addedTextTimeout) {
      //     clearTimeout(this.addedTextTimeout);
      //     this.addedTextTimeout = null;
      //   }
      //
      //   this.addedTextTimeout = setTimeout(() => {
      //     this.cart.innerHTML = this.theme.translations.addToCart;
      //   }, 2000);
      // } else if (cart_type === 'drawer' || cart_type === 'persistent') {
      // cart_action === drawer
      this.showCart();
      // }
    }
    this.showCart = () => {
      const cartDrawerTop = document.querySelector('.cart-drawer__top');
      let cartDrawerStatus = document.querySelector('.js-cart-drawer-status')

      cartDrawerStatus.textContent = `Item added to your cart`;
      cartDrawerTop.appendChild(cartDrawerStatus);
      const event = new CustomEvent('cart:open');
      document.querySelector('#cartSlideoutWrapper').dispatchEvent(event);
    }

    this.addToCartFailed = response => {
      // this.enableCartButton();
      let errorText = 'Unknown error';

      if (response.status == 0) {
        errorText = 'Unable to connect to server';
      } else if (response.status == 422) {
        errorText = response.description;
      } else if (response.responseJSON) {
        // Process JSON error
        if (response.responseJSON.description) {
          errorText = response.responseJSON.description;
        } else {
          errorText = response.responseJSON.message;
        }
      } else if (response.responseText) {
        // Use plain text error
        errorText = response.responseText;
      }

      this.showError(errorText);
    };

    this.showError = text => {
      let cartError = document.createElement('div');
      cartError.id = 'cart-error';
      cartError.classList.add('alert', 'alert-danger');
      cartError.textContent = text;
      this.cart.parentNode.insertBefore(cartError, this.cart.nextSibling);
    };

    this.clearError = () => {
      const cartError = document.querySelector('#cart-error');

      if (cartError) {
        cartError.parentNode.removeChild(cartError);
      }
    };
    this.updateCartDrawer = async function (cart, cart_element){
      if (! cart_element) {
        cart_element = $('#cartSlideoutWrapper')[0]
      }
      this.cart_element = cart_element
      if (! cart) return;

      const elementsToUpdate = this.cart_element.querySelectorAll('.ajax-cart--total-price, .cart-button-checkout, .additional-checkout-buttons, .taxes-and-shipping-message, .ajax-cart--checkout-add-note, .cart-price-text');
      const cartEmptyNotice = this.cart_element.querySelectorAll('.cart-empty-box');

      if (cart.items.length === 0) {
        // _this.showElements(cartEmptyNotice);
        // _this.hideElements(elementsToUpdate);
      } else {
        // _this.hideElements(cartEmptyNotice);
        // _this.showElements(elementsToUpdate);
      }

      let cartItems = this.cart_element.querySelector('.cart-items');

      cartItems.innerHTML = '';
      let self = _this;
      let cartData;

      try {
        const response = await fetch('/cart?view=compare');
        cartData = await response.json();
      } catch (e) {
        console.error('Cart fetch failed', e);
      }

      cart.items.forEach((item, index) => {
        let templateString = document.getElementById('cart-item-template').innerHTML;

        let template = _this.stringToHTML(templateString);

        let cartItem = template;
        let sellingPlanName = item.selling_plan_allocation ? item.selling_plan_allocation.selling_plan.name : null;
        const templateEls = {
          image: cartItem.querySelector('.cart-item-image'),
          title: cartItem.querySelector('.cart-item-product-title'),
          links: cartItem.querySelectorAll('.cart-item-link'),
          variant_title: cartItem.querySelector('.cart-item-variant-title'),
          price_per_unit: cartItem.querySelector('.cart-item-price-per-unit'),
          price: cartItem.querySelector('.cart-item-price'),
          selling_plan: cartItem.querySelector('.cart-item-selling-plan'),
          quantity: cartItem.querySelector('.cart-item-quantity'),
          quantity_wrapper: cartItem.querySelector('.cart-item-quantity').closest('.cart-item--quantity-wrapper'),
          discounts: cartItem.querySelector('.order-discount--cart-list'),
          original_price: cartItem.querySelector('.cart-item-price-original'),
          error_message: cartItem.querySelector('.errors')
        };
        let stockCount = parseInt(cartData.items[index].stock_count);
        let stockPolicy = cartData.items[index].stock_policy;
        let stockManagement = cartData.items[index].inventory_management;
        templateEls.quantity_wrapper.setAttribute('data-stock-count', stockCount.toString());
        templateEls.quantity_wrapper.setAttribute('data-inventory-policy', stockPolicy);
        templateEls.quantity_wrapper.setAttribute('data-inventory-management', stockManagement);

        let updateQuantity = quantity => {
          _this.cartSetQuantity(quantity, cartItem, templateEls.quantity_wrapper);
        };

        templateEls.title.title = item.product_title;
        templateEls.title.innerText = item.product_title;
        templateEls.links.forEach(element => element.setAttribute('href', item.url));
        templateEls.price.innerHTML = _this.formatMoney(item.final_line_price);
        templateEls.quantity.value = item.quantity.toString();
        templateEls.quantity.setAttribute('name', 'updates[]');
        templateEls.error_message.textContent = 'You can\'t add more of this item to the cart';
        templateEls.quantity.addEventListener('change', function (){
          let new_q = parseInt(this.value.toString(), 10);
          let getQuantity = parseInt(this.parentElement.dataset.stockCount, 10);
          let inventoryPolicy = this.parentElement.dataset.inventoryPolicy;
          let inventoryManagement = this.parentElement.dataset.inventoryManagement;

          if (new_q <= getQuantity || inventoryPolicy == "continue" || inventoryManagement != "shopify") {
            templateEls.quantity.value = new_q.toString();
            setTimeout(() => {
              updateQuantity(new_q.toString());
            }, 800);
          } else {
            templateEls.quantity.value = getQuantity.toString();
            templateEls.error_message.classList.remove('hide');
            setTimeout(() => {
              updateQuantity(getQuantity.toString());
            }, 800);
          }
        });

        if (item.image) {
          templateEls.image.setAttribute('src', item.image);
          templateEls.image.setAttribute('alt', item.featured_image.alt);
        } else {
          templateEls.image.classList.add('d-none');
        }

        if (item.variant_title) {
          templateEls.variant_title.classList.remove('hide');
          templateEls.variant_title.innerText = item.variant_title;
        }

        if (item.unit_price_measurement) {
          var priceString = '<span class="price">(' + _this.theme.formatMoney(item.unit_price) + ' / ' + (item.unit_price_measurement.reference_value != 1 ? item.unit_price_measurement.reference_value : '') + ' ' + item.unit_price_measurement.reference_unit + ')';
          templateEls.price_per_unit.innerHTML = priceString;
        }

        if (sellingPlanName) {
          templateEls.selling_plan.classList.remove('hide');
          templateEls.selling_plan.innerHTML = sellingPlanName;
        }

        if (item.original_line_price > item.final_line_price) {
          templateEls.original_price.innerHTML = _this.theme.formatMoney(item.original_line_price);
          templateEls.original_price.classList.add('d-block');
        } else {
          templateEls.original_price.classList.add('d-none');
        }

        cartItem.querySelectorAll('.cart-item-quantity-button').forEach(item => {
          item.addEventListener('click', function (){
            let current = parseInt(templateEls.quantity.value.toString(), 10);
            let change = parseInt(this.dataset.amount, 10);
            let new_q = current + change;
            self.statusString = change > 0 ? 'Added To Cart' : 'Removed From Cart';
            let getQuantity = parseInt(this.parentElement.dataset.stockCount, 10);
            let inventoryPolicy = this.parentElement.dataset.inventoryPolicy;
            let inventoryManagement = this.parentElement.dataset.inventoryManagement;

            if (new_q <= getQuantity || inventoryPolicy == "continue" || inventoryManagement != "shopify") {
              templateEls.quantity.value = new_q.toString();
              setTimeout(() => {
                updateQuantity(new_q);
              }, 800);
            } else {
              templateEls.quantity.value = getQuantity.toString();
              templateEls.error_message.classList.remove('hide');
              setTimeout(() => {
                updateQuantity(getQuantity);
              }, 800);
            }
          });
        });
        cartItem.querySelector('#cart-item-remove-button').addEventListener('click', function (){
          self.statusString = 'Removed From Cart';
          let new_q = 0;
          templateEls.quantity.value = new_q.toString();
          updateQuantity(new_q);
        });
        templateEls.discounts.innerHTML = '';
        item.line_level_discount_allocations.forEach(function (discount_allocation){
          const item = document.createElement("li");
          const discountItem = document.createElement("span");
          item.classList.add('order-discount--item');
          discountItem.classList.add('order-discount--item-title');
          discountItem.innerText = discount_allocation.discount_application.title;
          item.append(discountItem);
          const discountAmount = `<span class="sr-only">Savings</span> (- ${self.formatMoney(discount_allocation.amount)})`;
          const discountEl = document.createElement("span");
          discountEl.classList.add('order-discount--cart-amount');
          discountEl.innerHTML = discountAmount;
          item.append(discountEl);
          templateEls.discounts.append(item);
        });
        cartItems.append(cartItem);
      });
      if (self.statusString) _this.updateStatus(cart.total_price);

      if (_this.headerQuantity) {
        document.querySelectorAll('.cart-item-count-header').forEach(item => {
          item.textContent = cart.item_count.toString();
        });
      }

      document.querySelectorAll('.js-cart-total').forEach(item => {
        item.innerHTML = _this.formatMoney(cart.total_price);
      });
      var originalPrice = document.querySelector('.cart-item-original-total-price');

      if (cart.original_total_price > cart.total_price) {
        originalPrice.innerHTML = _this.formatMoney(cart.original_total_price);
        originalPrice.classList.add('d-block');
      } else {
        originalPrice.classList.remove('d-block');
        originalPrice.classList.add('d-none');
      }

      var cartDiscount = document.querySelector('.ajax-cart-discount-wrapper');
      cartDiscount.innerHTML = '';
      cart.cart_level_discount_applications.forEach(function (discount_application){
        const item = document.createElement("div");
        const orderDiscount = document.createElement("span");
        const totalDiscounted = self.formatMoney(discount_application.total_allocated_amount);
        const orderDiscountTitle = document.createElement("span");
        const minusSymbol = document.createElement("span");
        orderDiscount.classList.add('order-discount');
        orderDiscount.innerHTML = totalDiscounted;
        minusSymbol.innerText = '-';
        orderDiscount.querySelector('.money').prepend(minusSymbol);
        orderDiscountTitle.classList.add('order-discount--cart-title');
        orderDiscountTitle.innerText = discount_application.title;
        item.append(orderDiscount);
        item.append(orderDiscountTitle);
        item.classList.add('cart--order-discount-wrapper--indiv');
        cartDiscount.append(item);
      }); // Free shipping msg update

      const freeShippingContainer = document.querySelector("[data-free-shipping-msg]");

      if (freeShippingContainer) {
        // _this.theme.fetchSection('/', 'free-shipping-msg', freeShippingContainer);
      }
    };

    let self = this;
    // this.addToCartForm = this.element.querySelector('#add-to-cart-form');
    // this.product = JSON.parse(productScript.textContent);
    // this.cart = this.element.querySelector('#purchase');
    const autoload = true;

    if (this.cart) {
      this.cart.addEventListener('click', e => {
        e.preventDefault();
        self.disableCartButton();
        self.addToCart();
      });
    } // Handle thumbnail and media click

    this.stringToHTML = str => {
      const parser = new DOMParser();
      const doc = parser.parseFromString(str, 'text/html');
      return doc.body.querySelector('li');
    };
    this.formatMoney = (cents, format) => {
      let moneyFormat = this.moneyFormat;

      if (typeof cents === 'string') {
        cents = cents.replace('.', '');
      }

      let value = '';
      let placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;
      let formatString = format || moneyFormat;

      function formatWithDelimiters(number, precision, thousands, decimal){
        if (precision === null || precision === undefined) {
          precision = 2;
        }

        thousands = thousands || ',';
        decimal = decimal || '.';

        if (isNaN(number) || number == null) {
          return 0;
        }

        number = (number / 100.0).toFixed(precision);
        let parts = number.split('.');
        let dollarsAmount = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands);
        let centsAmount = parts[1] ? decimal + parts[1] : '';
        return dollarsAmount + centsAmount;
      }

      switch (formatString.match(placeholderRegex)[1]) {
        case 'amount':
          value = formatWithDelimiters(cents, 2);
          break;

        case 'amount_no_decimals':
          value = formatWithDelimiters(cents, 0);
          break;

        case 'amount_with_comma_separator':
          value = formatWithDelimiters(cents, 2, '.', ',');
          break;

        case 'amount_no_decimals_with_comma_separator':
          value = formatWithDelimiters(cents, 0, '.', ',');
          break;

        case 'amount_no_decimals_with_space_separator':
          value = formatWithDelimiters(cents, 0, ' ');
          break;
      }

      return `<span class="money"><span class="sr-only">${this.translations.cart_title} ${this.translations.cart_subtotal}</span>` + formatString.replace(placeholderRegex, value) + '</span>';
    }//end formatMoney
    this.cartSetQuantity = async function (quantity, cart_item, quantity_wrapper){
      const self = _this;
      const removeItem = quantity <= 0;
      let delay = removeItem ? _this.FADE_OUT_ANIMATION_DURATION : 0;
      const formData = new FormData(document.querySelector('.cart-drawer-form'));

      // _this.updateErrorMessage('');

      try {
        const response = await (await fetch('/cart/update.js', {
          method: "POST",
          body: formData
        })).json();

        if (response) {
          setTimeout(() => {
            self.updateCartDrawer(response);
          }, delay);
        }
      } catch (error) {
        console.error(error.message);
        // self.updateErrorMessage('Unable to update item quantity');
      } finally {
        /**
         * @todo Review business logic here - removing item even if request has failed
         */
        quantity_wrapper.classList.remove('cart-item-quantity-active');
        quantity_wrapper.classList.add('cart-item-quantity-active');

        if (removeItem) {
          cart_item.classList.add('fadeOut', 'animated', 'fast');
          setTimeout(function (){
            cart_item.remove();
          }, delay);
        }
      }
    }//end cartSetQuantity
    this.updateStatus = total_price => {
      const updatedStatus = `<span>'Cart Item' ${this.statusString} ${this.cartTitleText}, ${this.formatMoney(total_price)}</span>`;
      this.cartDrawerStatus.innerHTML = updatedStatus;
    }//end updatedStatus

  }//end constructor

}
