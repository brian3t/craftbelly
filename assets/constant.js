//Boxes in different sizes
const BASES = {
  vanilla: {
    name: 'Vanilla',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/vanilla_flavor.png?v=1663039008',
    shopify_id: 7510780575903,
    variant_id: 42556557426847,
    price: 30
  },
  coffee: {
    name: 'Coffee',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/coffee_flavor.png?v=1663039008',
    shopify_id: 7510780575903,
    variant_id: 42556557426847,
    price: 30
  },
  chocolate: {
    name: 'Chocolate',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/Chocolate_flavor.png?v=1663039007',
    shopify_id: 7510780575903,
    variant_id: 42556557426847,
    price: 30
  },
  strawberry: {
    name: 'Strawberry',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/strawberry_flavor.png?v=1663039009',
    shopify_id: 7510780575903,
    variant_id: 42556557426847,
    price: 30
  },
}
//labels
const S = {
  1: {
    name: 'Anniversary 1',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/anniversary_1.png?v=1663039006',
    tag: 'anniversary'
  },
  2: {
    name: 'Anniversary 2',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/anniversary_2.png?v=1663039007',
    tag: 'anniversary'
  },
  3: {
    name: 'Baby Shower 1',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/baby_shower_1.png?v=1663039007',
    tag: 'congrats'
  },
  4: {
    name: 'Baby Shower 2',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/baby_shower_2.png?v=1663039007',
    tag: 'congrats'
  },
  5: {
    name: 'Birthday 1',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/birthday_1.png?v=1663039007',
    tag: 'birthday'
  },
  6: {
    name: 'Birthday 2',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/birthday_2.png?v=1663039007',
    tag: 'birthday'
  },
  7: {
    name: 'Congratulations 1',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/congratulation_1.png?v=1663039007',
    tag: 'congrats'
  },
  8: {
    name: 'Congratulations 2',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/congratulation_2.png?v=1663039007',
    tag: 'congrats'
  },
  9: {
    name: 'Sorry 1',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/sorry_1.png?v=1663039007',
    tag: 'all'
  },
  10: {
    name: 'Sorry 2',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/sorry_2.png?v=1663039008',
    tag: 'all'
  },
  11: {
    name: 'Thank you 1',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/thank_you_1.png?v=1663039008',
    tag: 'all'
  },
  12: {
    name: 'Thank you 2',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/thank_you_2.png?v=1663039008',
    tag: 'all'
  },
}
//mixins
const C = {
  1: {
    name: 'Caramel',
    dtl_heading: 'Caramel',
    description: 'Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/caramel.png?v=1663037835',
    tag: 'crispy'
  },
  2: {
    name: 'Fudge',
    dtl_heading: 'Fudge',
    description: 'Fudge Fudge Fudge Fudge Fudge Fudge Fudge ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/fudge.png?v=1663037836',
    tag: 'crispy'
  },
  3: {
    name: 'Chocolate Chips',
    dtl_heading: 'Chocolate Chips',
    description: 'Chocolate Chips Chocolate Chips Chocolate Chips Chocolate Chips Chocolate Chips Chocolate Chips Chocolate Chips Chocolate Chips Chocolate Chips ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/chocolate_chip.png?v=1663037836',
    tag: 'crispy sweet'
  },
  4: {
    name: 'Apple Pie Filling',
    dtl_heading: 'Apple Pie Filling',
    description: 'Apple Pie Filling Apple Pie Filling Apple Pie Filling Apple Pie Filling Apple Pie Filling ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/apple_pie.png?v=1663037835',
    tag: 'crispy sweet'
  },
  5: {
    name: 'Banana',
    dtl_heading: 'Banana',
    description: 'Banana Banana Banana Banana Banana Banana Banana Banana Banana Banana ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/bananas.png?v=1663037835',
    tag: 'crispy'
  },
  6: {
    name: 'Blueberry',
    dtl_heading: 'Blueberry',
    description: 'Blueberry Blueberry Blueberry Blueberry Blueberry Blueberry Blueberry Blueberry Blueberry ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/blueberries.png?v=1663037834',
    tag: 'fruits'
  },
  7: {
    name: 'Brownies',
    dtl_heading: 'Brownies',
    description: 'Brownies Brownies Brownies Brownies Brownies Brownies Brownies Brownies Brownies ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/brownies.png?v=1663037835',
    tag: 'crispy'
  },
  8: {
    name: 'Peanut Butter',
    dtl_heading: 'Peanut Butter',
    description: 'Peanut Butter Peanut Butter Peanut Butter Peanut Butter Peanut Butter Peanut Butter ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/peanut_butter.png?v=1663037836',
    tag: 'sweet'
  },
  9: {
    name: 'Strawberry',
    dtl_heading: 'Strawberry',
    description: 'Strawberry Strawberry Strawberry Strawberry Strawberry Strawberry Strawberry Strawberry ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/strawberries.png?v=1663037837',
    tag: 'fruits'
  },
  10: {
    name: 'Reese',
    dtl_heading: 'Reese',
    description: 'Reese Reese Reese Reese Reese Reese Reese Reese Reese Reese Reese Reese Reese ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/reeses.png?v=1663037837',
    tag: 'fruits'
  },
  11: {
    name: 'Pecans',
    dtl_heading: 'Pecans',
    description: 'Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans Pecans ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/pecans.png?v=1663037836',
    tag: 'fruits'
  },
  12: {
    name: 'Pie Crust',
    dtl_heading: 'Pie Crust',
    description: 'Pie Crust Pie Crust Pie Crust Pie Crust Pie Crust Pie Crust Pie Crust ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/pie_crust.png?v=1663037836',
    tag: 'fruits'
  },
  13: {
    name: 'Coconut',
    dtl_heading: 'Coconut',
    description: 'Coconut Coconut Coconut Coconut Coconut Coconut Coconut Coconut Coconut ',
    img: 'https://cdn.shopify.com/s/files/1/0426/3070/9407/files/coconut.png?v=1663037836',
    tag: 'fruits'
  },
}
